#include "sprite2D.h"
#include "framework.h"
#include "ResourceManager.h"
#include "DirectXTK-master\Inc\WICTextureLoader.h"

bool ReadBinaryFile(const char* filename, BYTE** data, unsigned int& size) {
	FILE* fp = 0;
	if (fopen_s(&fp, filename, "rb"))
	{
		return false;
	}

	fseek(fp, 0, SEEK_END);

	size = ftell(fp);

	fseek(fp, 0, SEEK_SET);
	*data = new unsigned char[size];
	fread(*data, size, 1, fp);

	fclose(fp);

	return true;
}



ResourceManager::ResourceManager() {

}

ResourceManager::~ResourceManager() {

}

ResourceManager::RESOURCE_INFO	ResourceManager::ResourceInfo[ResourceManager::RESOURCE_MAX];
ResourceManager::RESOURCE_VERTEXSHADER	ResourceManager::ResourceVertexShader[ResourceManager::RESOURCE_MAX];
ResourceManager::RESOURCE_PIXELSHADER	ResourceManager::ResourcePixcelShader[ResourceManager::RESOURCE_MAX];

void ResourceManager::Release() {
	for (int i = 0; i < RESOURCE_MAX; i++)
	{
		ResourceInfo[i].Release(true);
		ResourceVertexShader[i].Release(true);
		ResourcePixcelShader[i].Release(true);
	}
}

bool ResourceManager::LoadShaderResourceView(ID3D11Device* p_Device, const wchar_t* fileName, ID3D11ShaderResourceView** p_ShaderResourceView, D3D11_TEXTURE2D_DESC* p_TEXTURE2D_DESC) {
	RESOURCE_INFO* p_Find = NULL;
	ID3D11Resource* p_Resource = NULL;
	int no = -1;//最初に(番号)
	//////////////////////////
	//データ検索
	for (int i = 0; i < RESOURCE_MAX; i++) {
		RESOURCE_INFO* p = &ResourceInfo[i];
		//データがないなら虫
		//ただし最初に見つけた空き領域ならセット用に確保
		if (p->counter == 0) {
			if (no = -1)no = i;
			continue;
		}
		//ファイルパスが違うなら無視
		if (wcscmp(p->fileName, fileName) != 0)continue;
		//同名ファイルが存在するなら
		p_Find = p;//発見
		p->p_ShaderResourceView->GetResource(&p_Resource);//Resourceの準備
		break;
	}

	////////////////////////////////////////////////
	// データが見つからなかった = 新規読み込み
	if (!p_Find) {
		RESOURCE_INFO* p = &ResourceInfo[no];
		HRESULT hr = DirectX::CreateWICTextureFromFile(p_Device, fileName, &p_Resource, (&p->p_ShaderResourceView));
		if (FAILED(hr))
			return false;
		p_Find = p;//発見
		wcscpy_s(p->fileName, 256, fileName);//名前コピー
	}

	////////////////////////////////////////////////
	// 最終処理(参照渡しでデータを返す)
	ID3D11Texture2D* Tex2D;//テクスチャデータの準備
	p_Resource->QueryInterface(&Tex2D);
	// データをコピー
	*p_ShaderResourceView = p_Find->p_ShaderResourceView;
	Tex2D->GetDesc(p_TEXTURE2D_DESC);
	// 数を追加
	p_Find->counter++;
	// 解放
	Tex2D->Release();
	p_Resource->Release();
	return true;
}

bool ResourceManager::LoadVertexShader(ID3D11Device* p_Device, const char* csofileName, D3D11_INPUT_ELEMENT_DESC* p_INPUT_ELEMENT_DESC, const UINT ELEMENTS_ARRAY_NUM, ID3D11VertexShader** p_VertexShader, ID3D11InputLayout** p_InputLayout) {
	//初期化としてのNULL代入
	*p_VertexShader = NULL;
	*p_InputLayout = NULL;
	RESOURCE_VERTEXSHADER* p_Find = NULL;
	int no = -1;
	///////////////////////
	//const char*->wchar_t*(検索保存で使用)
	wchar_t fileName[256];
	size_t stringSize = 0;
	mbstowcs_s(&stringSize, fileName, csofileName, strlen(csofileName));
	//////////////////////
	//データ検索
	for (int n = 0; n < RESOURCE_MAX; n++) {
		RESOURCE_VERTEXSHADER* p = &ResourceVertexShader[n];
		//データがないなら虫
		//ただし、最初に見つけた空き領域なら確保
		if (p->counter == 0) {
			if (no == -1) {
				no = n;
				continue;
			}
		}
		//ファイルパスが違うなら虫
		if (wcscmp(p->fileName, fileName) != 0)continue;
		//同名ファイルが存在した
		p_Find = p;//発見
		break;
	}

	////////////////////////////////////////////////
	// データが見つからなかった = 新規読み込み
	if (!p_Find) {
		RESOURCE_VERTEXSHADER* p = &ResourceVertexShader[no];
		BYTE* data;//頂点シェーダーデータ
		UINT size;//頂点シェーダーデータサイズ
		// コンパイル済み頂点シェーダーオブジェクトの読み込み
		if (!ReadBinaryFile(csofileName, &data, size))return false;

		// 頂点シェーダーオブジェクトの生成
		HRESULT hr = p_Device->CreateVertexShader(data, size, nullptr, &p->p_VertexShader);
		if (FAILED(hr)) {
			delete[] data;
			return false;
		}

		// 入力レイアウトの作成
		hr = p_Device->CreateInputLayout(p_INPUT_ELEMENT_DESC, ELEMENTS_ARRAY_NUM, data, size, &p->p_InputLayout);
		delete[] data;
		if (FAILED(hr))
			return false;

		// 新規データの保存
		p_Find = p;//発見
		wcscpy_s(p->fileName, 256, fileName);//名前コピー
	}
	////////////////////////////////////////////////
	// 最終処理(参照渡しでデータを返す)
	*p_VertexShader = p_Find->p_VertexShader;
	*p_InputLayout = p_Find->p_InputLayout;
	p_Find->counter++;
	return true;
}


bool ResourceManager::LoadPixelShader(ID3D11Device* p_Device, const char* csofileName, ID3D11PixelShader** p_PixelShader) {
	//初期化としてのNULL代入
	*p_PixelShader = NULL;
	RESOURCE_PIXELSHADER* p_Find = NULL;
	int no = -1;
	///////////////////////
	//const char*->wchar_t*(検索保存で使用)
	wchar_t fileName[256];
	size_t stringSize = 0;
	mbstowcs_s(&stringSize, fileName, csofileName, strlen(csofileName));
	//////////////////////
	// データ検索
	for (int n = 0; n < RESOURCE_MAX; n++)
	{
		RESOURCE_PIXELSHADER* p = &ResourcePixcelShader[n];
		//データがないなら虫
		//ただし、最初に見つけた空き領域なら確保
		if (p->counter == 0)
		{
			if (no == -1)
			{
				no = n;
				continue;
			}
		}
		//ファイルパスが違うなら虫
		if (wcscmp(p->fileName, fileName) != 0)
			continue;


		//同名ファイルが存在した
		p_Find = p;//発見
		break;

	}

	////////////////////////////////////////////////
	// データが見つからなかった = 新規読み込み
	if (!p_Find)
	{
		RESOURCE_PIXELSHADER* p = &ResourcePixcelShader[no];
		BYTE* data;//ピクセルシェーダデータ
		UINT size;//ピクセルシェーダデータサイズ
		// コンパイル済み頂点シェーダーオブジェクトの読み込み
		if (!ReadBinaryFile(csofileName, &data, size))return false;
		// ピクセルシェーダーオブジェクトの生成
		HRESULT hr = p_Device->CreatePixelShader(data, size, nullptr, &p->p_PixelShader);
		delete[] data;
		if (FAILED(hr)) {
			return false;
		}
		// 新規データの保存
		p_Find = p;//発見
		wcscpy_s(p->fileName, 256, fileName);//名前コピー
	}
	////////////////////////////////////////////////
	// 最終処理(参照渡しでデータを返す)
	*p_PixelShader = p_Find->p_PixelShader;
	p_Find->counter++;
	return true;
}

void ResourceManager::ReleaseShaderResourceView(ID3D11ShaderResourceView* p_ShaderResourceView) {
	if (!p_ShaderResourceView)return;
	for (int n = 0; n < RESOURCE_MAX; n++)
	{
		RESOURCE_INFO* p = &ResourceInfo[n];
		//データがないなら虫
		if (p->counter == 0)continue;
		//データが違うなら虫
		if (p_ShaderResourceView != p->p_ShaderResourceView)continue;
		//データが存在した
		p->Release();//データ開放
		break;
	}
}

void ResourceManager::ReleaseVertexShader(ID3D11VertexShader* p_VertexShader, ID3D11InputLayout* p_InputLayout)
{

	if (!p_VertexShader)
		return;

	for (int n = 0; n < RESOURCE_MAX; n++)
	{
		RESOURCE_VERTEXSHADER* p = &ResourceVertexShader[n];
		//データがないなら虫
		if (p->counter == 0)continue;
		//データが違うなら虫
		if (p_VertexShader != p->p_VertexShader)continue;
		//データが存在した
		p->Release();//データ開放
		break;
	}
}

void ResourceManager::ReleasePixelShader(ID3D11PixelShader* p_PixelShader)
{
	if (!p_PixelShader)
		return;

	for (int n = 0; n < RESOURCE_MAX; n++)
	{
		RESOURCE_PIXELSHADER* p = &ResourcePixcelShader[n];
		//データがないなら虫
		if (p->counter == 0)continue;
		//データが違うなら虫
		if (p_PixelShader != p->p_PixelShader)continue;
		//データが存在した
		p->Release();//データ開放
		break;
	}

}
#pragma once
#include <d3d11.h>//追加
#include "sprite2D.h"
#include <string>
//////////////////////////////////////////////
//Unit7
////////////////////////////////////////////////
class BlendMode {
public:
	enum BLEND_MODE {
		NONE = 0,//合成なし（デフォルト）
		ALPHA,	    //α合成
		ADD,	    //加算合成
		SUB,	    //減産合成
		REPLACE,    //置き換え
		MULTIPLY,   //乗算
		LIGHTEN,    //比較（明）
		DARKEN,	    //比較（暗）
		SCREEN,	    //スクリーン
		MODE_MAX,   //
	};
private:
	static ID3D11BlendState* BlendState[MODE_MAX];//ブレンド設定配列
	static bool bLoad;//true:設定配列作成済み
	static BLEND_MODE enumMode;//現在使用してるブレンドモード
	BlendMode() {};
	~BlendMode() { Release(); };
public:
	static bool Initializer(ID3D11Device* p_Device);
	static void Release() {
		for (BLEND_MODE mode = NONE; mode < MODE_MAX; mode = (BLEND_MODE)(mode + 1)) {
			BlendState[mode]->Release();
		}
		bLoad = false;
	}
	//ブレンド設定用関数
	static void Set(ID3D11DeviceContext* p_DeviceContext, BLEND_MODE mode = NONE) {
		if (!bLoad)return;
		if (mode < 0 || mode >= MODE_MAX) return;
		if (mode == enumMode)return;
		p_DeviceContext->OMSetBlendState(BlendState[mode], nullptr, 0xffffffff);
		enumMode = mode;
	}
	//static void BlendSwitch(D3D11_BLEND_OP mode);//ブレンドモードスイッチ
	//static void BlendAlpha();
	//static void BlendAdd();
	//static void BlendSubtract();
	//static void BlendReplace();
	//static void BlendMultiply(D3D11_BLEND mode1, D3D11_BLEND mode2);
	//static void BlendLighten();
	//static void BlendDarken();
	//static void BlendScreen();
private:
	/*D3D11_BLEND dst;
	D3D11_BLEND src;*/
	/* static DirectX::XMFLOAT4 dst;
	static DirectX::XMFLOAT4 src;
	static DirectX::XMFLOAT4 res;*/
};


#pragma once

#include <windows.h>
#include <tchar.h>
#include <sstream>

#include "misc.h"
#include "high_resolution_timer.h"

#include <d3d11.h>//追加
#include <dxgi.h>
//#include "sprite2D.h"
#include "Blend.h"
#include "Font.h"
#include "GeometricPrimitive.h"
// unit12
#include "Static_mesh.h"

#pragma comment (lib,"d3d11.lib")
#pragma comment (lib,"dxgi.lib")



class ResourceManager;
class framework
{
public:
	CONST HWND hwnd;
	static CONST LONG SCREEN_WIDTH = 1280;
	static CONST LONG SCREEN_HEIGHT = 720;
	ID3D11Device* p_Device;
	ID3D11DeviceContext* p_DeviceContext;
	IDXGISwapChain* p_SwapChain;
	ID3D11RenderTargetView* p_RenderTargetView;
	ID3D11DepthStencilView* p_DepthStencilView;
	////////////////////////////////////////
	//Added by Unit7
	///////////////////////////////////////
	ID3D11BlendState* p_BlendState;
	////////////////////////////////////////
	//Added by Unit10
	///////////////////////////////////////
	GeometricPrimitive *cube;
	Static_mesh* mesh;

	////////////////////////////////////////
	//Castamed by Unit8
	///////////////////////////////////////
	framework(HWND hwnd) : hwnd(hwnd), p_Device(nullptr),p_DeviceContext(nullptr),p_SwapChain(nullptr),p_RenderTargetView(nullptr),
	    p_DepthStencilView(nullptr)/*,particle(nullptr)*/
	{
	}
#define RELEASE_IF(x) if(x){x->Release();x=nullptr;}
#define DELETE_IF(x) if(x){delete x; x=NULL;}
	~framework()
	{
		// デバイスステートのクリア
		if (p_DeviceContext)p_DeviceContext->ClearState();
	    Font::ReleaseImage();
		BlendMode::Release();

		DELETE_IF(cube);
		DELETE_IF(mesh);
	    ///DELETE_IF(particle);
	    ///delete[] benchmarkTime;
	   /* for (int i = 0; i < 1024; i++) {
		DELETE_IF(sprites[i]);
	    }*/
	    ////////////////////////////////////////
	    //Added by Unit7
	    ///////////////////////////////////////
		// 取得したインターフェースのクリア
	    ///RELEASE_IF(p_BlendState);
	    RELEASE_IF(p_DepthStencilView);
	    RELEASE_IF(p_RenderTargetView);
	    RELEASE_IF(p_SwapChain);
	    RELEASE_IF(p_DeviceContext);
	    RELEASE_IF(p_Device);
	    ///ResourceManager::Release();
	}
#undef RELEASE_IF
#undef DELETE_IF
	int run()
	{
		MSG msg = {};

		if (!initialize()) return 0;
		while (WM_QUIT != msg.message)
		{
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
			{
				timer.tick();
				calculate_frame_stats();
				update(timer.time_interval());
				render(timer.time_interval());
			}
		}
		return static_cast<int>(msg.wParam);
	}

	LRESULT CALLBACK handle_message(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
	{
		switch (msg)
		{
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc;
			hdc = BeginPaint(hwnd, &ps);
			EndPaint(hwnd, &ps);
			break;
		}
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		case WM_CREATE:
			break;
		case WM_KEYDOWN:
			if (wparam == VK_ESCAPE) PostMessage(hwnd, WM_CLOSE, 0, 0);
			break;
		case WM_ENTERSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
			timer.stop();
			break;
		case WM_EXITSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
			// Here we reset everything based on the new window dimensions.
			timer.start();
			break;
		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
		}
		return 0;
	}

private:
	bool initialize();
	void update(float elapsed_time/*Elapsed seconds from last frame*/);
	void render(float elapsed_time/*Elapsed seconds from last frame*/);
	// 無駄な画面描画の抑制
	bool StanbyPresant();//! 追加

private:
	high_resolution_timer timer;
	void calculate_frame_stats()
	{
		// Code computes the average frames per second, and also the
		// average time it takes to render one frame.  These stats
		// are appended to the window caption bar.
		static int frames = 0;
		static float time_tlapsed = 0.0f;

		frames++;

		// Compute averages over one second period.
		if ((timer.time_stamp() - time_tlapsed) >= 1.0f)
		{
			float fps = static_cast<float>(frames); // fps = frameCnt / 1
			float mspf = 1000.0f / fps;
			std::ostringstream outs;
			outs.precision(6);
			outs << "FPS : " << fps << " / " << "Frame Time : " << mspf << " (ms)";
			SetWindowTextA(hwnd, outs.str().c_str());

			// Reset for next average.
			frames = 0;
			time_tlapsed += 1.0f;
		}
	}
};

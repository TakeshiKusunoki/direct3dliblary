#include "GeometricPrimitive.h"
#include "ResourceManager.h"




GeometricPrimitive::GeometricPrimitive(ID3D11Device *p_Device){
	HRESULT hr = S_OK;

	/////////////////////////////////////////////////
	// �@頂点データの構造を記述(記載した情報をIAステージに伝える)
	/////////////////////////////////////////////////
	D3D11_INPUT_ELEMENT_DESC InputElementDesk[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	const UINT ELEMENTS_ARRAY_NUM = ARRAYSIZE(InputElementDesk);
	/////////////////////////////////////////////////
	// �Aバーテックスシェーダーオブジェクトの生成
	/////////////////////////////////////////////////
	ResourceManager::LoadVertexShader(p_Device, "geometric_primitive_vs.cso", InputElementDesk, ELEMENTS_ARRAY_NUM, &p_VertexShader, &p_InputLayout);
	/////////////////////////////////////////////////
	// �Bピクセルシェーダーオブジェクトの生成
	/////////////////////////////////////////////////
	ResourceManager::LoadPixelShader(p_Device, "geometric_primitive_ps.cso", &p_PixelShader);

	////////////////////////////////////////////////////////
	// �Cラスタライザーステートオブジェクトの生成（線描画・塗りつぶし描画)
	////////////////////////////////////////////////////////
	D3D11_RASTERIZER_DESC RasteriserDesk;
	ZeroMemory(&RasteriserDesk, sizeof(D3D11_RASTERIZER_DESC));
	//-線描画の場合
	RasteriserDesk.FillMode = D3D11_FILL_WIREFRAME;	//レンダリング時に使用する描画モードを決定します
	RasteriserDesk.CullMode = D3D11_CULL_NONE;		//特定の方向を向いている三角形の描画の有無を示します。
	RasteriserDesk.FrontCounterClockwise = true;	//三角形が前向きか後ろ向きかを決定します。
	RasteriserDesk.DepthBias = 0;					//指定のピクセルに加算する深度値です。
	RasteriserDesk.DepthBiasClamp = 0;				//ピクセルの最大深度バイアスです。
	RasteriserDesk.SlopeScaledDepthBias = 0;		//指定のピクセルのスロープに対するスカラです。
	RasteriserDesk.DepthClipEnable = false;			//距離に基づいてクリッピングを有効にします。
	RasteriserDesk.ScissorEnable = false;			//シザーカリング
	RasteriserDesk.MultisampleEnable = false;		//マルチサンプリングのアンチエイリアシング
	RasteriserDesk.AntialiasedLineEnable = true;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
	hr = p_Device->CreateRasterizerState(&RasteriserDesk, &p_RasterizerStateLine);
	if (FAILED(hr))
	{
		assert(!"ラスタライザーステートオブジェクトの生成ができません");
		return;
	}

	//-塗りつぶし描画の場合
	RasteriserDesk.FillMode = D3D11_FILL_SOLID;		//レンダリング時に使用する描画モードを決定します
	RasteriserDesk.CullMode = D3D11_CULL_BACK;		//特定の方向を向いている三角形の描画の有無を示します。
	RasteriserDesk.MultisampleEnable = true;		//マルチサンプリングのアンチエイリアシング
	RasteriserDesk.AntialiasedLineEnable = false;	//線のアンチエイリアシング(線を描画中で MultisampleEnable が false の場合にのみ)
	hr = p_Device->CreateRasterizerState(&RasteriserDesk, &p_RasterizerStatePaint);
	if (FAILED(hr))
	{
		assert(!"ラスタライザーステートオブジェクト(塗りつぶし描画)の生成ができません");
		return;
	}

	/////////////////////////////////////////////////
	// �D深度ステンシル ステート オブジェクトの生成
	/////////////////////////////////////////////////
	D3D11_DEPTH_STENCIL_DESC DepthDesc;
	ZeroMemory(&DepthDesc, sizeof(DepthDesc));
	DepthDesc.DepthEnable = true;									//深度テストあり
	DepthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;			//書き込む
	DepthDesc.DepthFunc = D3D11_COMPARISON_LESS;					//手前の物体を描画
	DepthDesc.StencilEnable = FALSE;								//ステンシル テストなし
	DepthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;		//ステンシル読み込みマスク
	DepthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;	//ステンシル書き込みマスク
	//面が表を向いている場合のステンシルステートの設定
	DepthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
	DepthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
	//面が裏を向いている場合のステンシルステートの設定
	DepthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	  //維持
	DepthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		  //維持
	DepthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;		  //常に成功
	hr = p_Device->CreateDepthStencilState(&DepthDesc, &p_DepthStencilState);	//
	if (FAILED(hr)) {
		assert(!"深度ステンシル ステート オブジェクトの生成ができません");
		return;
	}


}









#define DELETE_IF(x) if(x){delete x;}
#define RELEASE_IF(x) if(x){x->Release();}
GeometricPrimitive::~GeometricPrimitive() {
	RELEASE_IF(p_DepthStencilState);
	RELEASE_IF(p_RasterizerStateLine);
	RELEASE_IF(p_RasterizerStatePaint);
	RELEASE_IF(p_BufferConst);
	RELEASE_IF(p_BufferIndex);
	RELEASE_IF(p_BufferVs);
	ResourceManager::ReleasePixelShader(p_PixelShader);
	ResourceManager::ReleaseVertexShader(p_VertexShader, p_InputLayout);
}
#undef RELEASE_IF
#undef DELETE_IF


void GeometricPrimitive::create_buffer(ID3D11Device * p_Device, VERTEX3D* vertices, const int NUM_VRETEX, UINT* indices, const int NUM_INDEX) {
	HRESULT hr = S_OK;
	/////////////////////////////////////////////////
	// �F頂点バッファオブジェクトの生成
	//////////////////////////////////////////////////
	// �E頂点情報・インデックス情報のセット// 一辺が 1.0 の正立方体データを作成する（重心を原点にする

	// 頂点バッファ定義
	D3D11_BUFFER_DESC Bufferdesk;
	ZeroMemory(&Bufferdesk, sizeof(Bufferdesk));
	Bufferdesk.ByteWidth = NUM_VRETEX * sizeof(VERTEX3D);
	Bufferdesk.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
	Bufferdesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	Bufferdesk.CPUAccessFlags = 0;
	Bufferdesk.MiscFlags = 0;
	Bufferdesk.StructureByteStride = 0;//float?sizeof(DirectX::XMFLOAT3)

									   // サブリソースの初期化に使用されるデータを指定します。
	D3D11_SUBRESOURCE_DATA SubResourceData;
	ZeroMemory(&SubResourceData, sizeof(SubResourceData));
	SubResourceData.pSysMem = vertices;				//(バッファの初期値)初期化データへのポインターです。
	SubResourceData.SysMemPitch = 0;					//テクスチャーにある 1 本の線の先端から隣の線までの距離 (バイト単位) です。
	SubResourceData.SysMemSlicePitch = 0;				//1 つの深度レベルの先端から隣の深度レベルまでの距離 (バイト単位) です。
	// バッファー (頂点バッファー、インデックス バッファー、またはシェーダー定数バッファー) を作成します。
	hr = p_Device->CreateBuffer(&Bufferdesk, &SubResourceData, &p_BufferVs);
	if (FAILED(hr))
	{
		assert(!"頂点バッファの作成ができません");
		return;
	}

	///////////////////////////////////////////////////
	// �Gインデックスバッファオブジェクトの生成
	///////////////////////////////////////////////////

	// インデックスバッファの定義
	D3D11_BUFFER_DESC IndexBufferDesc;
	ZeroMemory(&IndexBufferDesc, sizeof(IndexBufferDesc));
	IndexBufferDesc.ByteWidth = NUM_INDEX * sizeof(UINT);
	IndexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;	//GPUのみ
	IndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	IndexBufferDesc.CPUAccessFlags = 0;
	IndexBufferDesc.MiscFlags = 0;
	IndexBufferDesc.StructureByteStride = 0;
	//インデックスの数を補完
	numIndices = NUM_INDEX;

	// インデックス・バッファのサブリソースの定義
	D3D11_SUBRESOURCE_DATA IndexSubResource;
	ZeroMemory(&IndexSubResource, sizeof(IndexSubResource));
	IndexSubResource.pSysMem = indices;
	IndexSubResource.SysMemPitch = 0;
	IndexSubResource.SysMemSlicePitch = 0;

	// インデックス・バッファの作成
	hr = p_Device->CreateBuffer(&IndexBufferDesc, &IndexSubResource, &p_BufferIndex);
	if (FAILED(hr))
	{
		assert(!"インデックス・バッファの作成ができません");
		return;
	}

	// �H定数バッファオブジェクトの生成
	D3D11_BUFFER_DESC ConstantBufferDesc;
	ZeroMemory(&ConstantBufferDesc, sizeof(ConstantBufferDesc));
	ConstantBufferDesc.Usage = D3D11_USAGE_DEFAULT;			//動的使用法
	ConstantBufferDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;//定数バッファ
	ConstantBufferDesc.CPUAccessFlags = 0;//CPUから書き込む
	ConstantBufferDesc.MiscFlags = 0;
	ConstantBufferDesc.ByteWidth = sizeof(DirectX::XMFLOAT4X4);
	ConstantBufferDesc.StructureByteStride = 0;


	hr = p_Device->CreateBuffer(&ConstantBufferDesc, nullptr, &p_BufferConst);
	if (FAILED(hr))
	{
		assert(!"定数バッファオブジェクトの生成ができません");
		return;
	}
}







//定数
#define VERTEX_BUFFER_NUM 1//頂点バッファの数

void GeometricPrimitive::render(ID3D11DeviceContext *p_DeviceContext,   //デバイスコンテキスト
	const DirectX::XMFLOAT4X4 &wvp,	//ワールド・ビュー・プロジェクション合成行列
	const DirectX::XMFLOAT4X4 &world, //ワールド変換行列
	const DirectX::XMFLOAT4 &lightVector,  //ライト進行方向
	const DirectX::XMFLOAT4 &materialColor,  //材質色
	bool  FlagPaint					//線・塗りつぶし描画フラグ
) {


	// �@IAに頂点バッファを設定
	UINT stride[VERTEX_BUFFER_NUM] = { sizeof(VERTEX3D) };	 //頂点バッファにふくまれる頂点データのサイズ。
	UINT offset[VERTEX_BUFFER_NUM] = { 0 };					 //頂点バッファのオフセット
	p_DeviceContext->IASetVertexBuffers(0, VERTEX_BUFFER_NUM, &p_BufferVs, stride, offset);
	// �AIAにインデックスバッファを設定
	p_DeviceContext->IASetIndexBuffer(p_BufferIndex, DXGI_FORMAT_R32_UINT, 0);



	// 描画するプリミティブ種類の設定
	p_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);//３角形リストを描画する
																				   // 入力レイアウト・オブジェクトの設定
	p_DeviceContext->IASetInputLayout(p_InputLayout);

	// �Dシェーダーの設定;
	p_DeviceContext->VSSetShader(p_VertexShader, nullptr, 0);
	p_DeviceContext->PSSetShader(p_PixelShader, nullptr, 0);
	// �Cラスタライザ・ステート・オブジェクトの設定
	p_DeviceContext->RSSetState((FlagPaint ? p_RasterizerStatePaint : p_RasterizerStateLine));//ラスタライザステート
	///p_DeviceContext->RSSetScissorRects();//シザー短形



	// �Bコンスタントバッファを設定+		light_direction	{x=0.000000000 y=-1.00000000 z=0.000000000 ...}	DirectX::XMFLOAT4

	CONSTANT_BUFFER data;
	data.wvp = wvp;
	data.world = world;
	data.light_direction = lightVector;
	data.material_color = materialColor;
	// ? p_DeviceContext->Map();
	// ? p_DeviceContext->Unmap();
	p_DeviceContext->UpdateSubresource(p_BufferConst, 0, nullptr, &data, 0, 0);//情報を定数バッファへコピー
																			   //? p_DeviceContext->PSSetConstantBuffers();
																			   //? p_DeviceContext->GSSetConstantBuffers();
	p_DeviceContext->VSSetConstantBuffers(0, VERTEX_BUFFER_NUM, &p_BufferConst);//定数バッファをシェーダへセット


	//震度ステンシルステート
	p_DeviceContext->OMSetDepthStencilState(p_DepthStencilState, 0);//震度ステンシルステート

	//�Eインデックス付けされているプリミティブの描画
	/*D3D11_BUFFER_DESC BufferDesc;
	p_BufferIndex->GetDesc(&BufferDesc);*/
	p_DeviceContext->DrawIndexed(numIndices/*/sizeof(UINT)*/, 0, 0);

}

#undef VERTEX_BUFFER_NUM









//+ 正方形
Geometric_cube::Geometric_cube(ID3D11Device *p_Device) : GeometricPrimitive(p_Device) {
	using namespace DirectX;
	VERTEX3D Vertices[24] = {};
	UINT indices[36] = {};
	// 底面
	//0--1
	//2--3
	int face = 0;
	const XMFLOAT3 V = {0.5f, 0.5f, 0.5f};
	XMFLOAT3 N = { 0.0f, -1.0f, 0.0f };
	Vertices[face * 4 + 0].position = XMFLOAT3(-V.x, -V.y, V.z);
	Vertices[face * 4 + 1].position = XMFLOAT3(V.x, -V.y, V.z);
	Vertices[face * 4 + 2].position = XMFLOAT3(-V.x, -V.y, -V.z);
	Vertices[face * 4 + 3].position = XMFLOAT3(V.x, -V.y, -V.z);
	for (int i = 0; i < 4; i++)
	{
		Vertices[face * 4 + i].normal = N;
	}
	indices[face * 6 + 0] = face * 4 + 0;
	indices[face * 6 + 1] = face * 4 + 2;
	indices[face * 6 + 2] = face * 4 + 1;
	indices[face * 6 + 3] = face * 4 + 1;
	indices[face * 6 + 4] = face * 4 + 3;
	indices[face * 6 + 5] = face * 4 + 2;

	// 上面
	//0--1
	//2--3
	face++;
	N = { 0.0f, 1.0f, 0.0f };
	Vertices[face * 4 + 0].position = XMFLOAT3(-V.x, V.y, V.z);
	Vertices[face * 4 + 1].position = XMFLOAT3(V.x, V.y, V.z);
	Vertices[face * 4 + 2].position = XMFLOAT3(-V.x, V.y, -V.z);
	Vertices[face * 4 + 3].position = XMFLOAT3(V.x, V.y, -V.z);
	for (int i = 0; i < 4; i++)
	{
		Vertices[face * 4 + i].normal = N;
	}
	indices[face * 6 + 0] = face * 4 + 0;
	indices[face * 6 + 1] = face * 4 + 1;
	indices[face * 6 + 2] = face * 4 + 2;
	indices[face * 6 + 3] = face * 4 + 1;
	indices[face * 6 + 4] = face * 4 + 3;
	indices[face * 6 + 5] = face * 4 + 2;

	// 前面
	//0--1
	//2--3
	face++;
	N = { 0.0f, 0.0f, -1.0f };
	Vertices[face * 4 + 0].position = XMFLOAT3(-V.x, V.y, -V.z);
	Vertices[face * 4 + 1].position = XMFLOAT3(V.x, V.y, -V.z);
	Vertices[face * 4 + 2].position = XMFLOAT3(-V.x, -V.y, -V.z);
	Vertices[face * 4 + 3].position = XMFLOAT3(V.x, -V.y, -V.z);
	for (int i = 0; i < 4; i++)
	{
		Vertices[face * 4 + i].normal = N;
	}
	indices[face * 6 + 0] = face * 4 + 0;
	indices[face * 6 + 1] = face * 4 + 1;
	indices[face * 6 + 2] = face * 4 + 2;
	indices[face * 6 + 3] = face * 4 + 1;
	indices[face * 6 + 4] = face * 4 + 3;
	indices[face * 6 + 5] = face * 4 + 2;

	// 背面
	//0--1
	//2--3
	face++;
	N = { 0.0f, 0.0f, 1.0f };
	Vertices[face * 4 + 0].position = XMFLOAT3(-V.x, V.y, V.z);
	Vertices[face * 4 + 1].position = XMFLOAT3(V.x, V.y, V.z);
	Vertices[face * 4 + 2].position = XMFLOAT3(-V.x, -V.y, V.z);
	Vertices[face * 4 + 3].position = XMFLOAT3(V.x, -V.y, V.z);
	for (int i = 0; i < 4; i++)
	{
		Vertices[face * 4 + i].normal = N;
	}
	indices[face * 6 + 0] = face * 4 + 0;
	indices[face * 6 + 1] = face * 4 + 2;
	indices[face * 6 + 2] = face * 4 + 1;
	indices[face * 6 + 3] = face * 4 + 1;
	indices[face * 6 + 4] = face * 4 + 2;
	indices[face * 6 + 5] = face * 4 + 3;

	//右側
	//0--1
	//2--3
	face++;
	N = { 0.0f, 0.0f, 1.0f };
	Vertices[face * 4 + 0].position = XMFLOAT3(V.x, V.y, -V.z);
	Vertices[face * 4 + 1].position = XMFLOAT3(V.x, V.y, V.z);
	Vertices[face * 4 + 2].position = XMFLOAT3(V.x, -V.y, -V.z);
	Vertices[face * 4 + 3].position = XMFLOAT3(V.x, -V.y, V.z);
	for (int i = 0; i < 4; i++)
	{
		Vertices[face * 4 + i].normal = N;
	}
	indices[face * 6 + 0] = face * 4 + 0;
	indices[face * 6 + 1] = face * 4 + 1;
	indices[face * 6 + 2] = face * 4 + 2;
	indices[face * 6 + 3] = face * 4 + 1;
	indices[face * 6 + 4] = face * 4 + 3;
	indices[face * 6 + 5] = face * 4 + 2;

	//左側
	//0--1
	//2--3
	face++;
	N = { 0.0f, 0.0f, 1.0f };
	Vertices[face * 4 + 0].position = XMFLOAT3(-V.x, V.y, -V.z);
	Vertices[face * 4 + 1].position = XMFLOAT3(-V.x, V.y, V.z);
	Vertices[face * 4 + 2].position = XMFLOAT3(-V.x, -V.y, -V.z);
	Vertices[face * 4 + 3].position = XMFLOAT3(-V.x, -V.y, V.z);
	for (int i = 0; i < 4; i++)
	{
		Vertices[face * 4 + i].normal = N;
	}
	indices[face * 6 + 0] = face * 4 + 0;
	indices[face * 6 + 1] = face * 4 + 2;
	indices[face * 6 + 2] = face * 4 + 1;
	indices[face * 6 + 3] = face * 4 + 1;
	indices[face * 6 + 4] = face * 4 + 2;
	indices[face * 6 + 5] = face * 4 + 3;

	create_buffer(p_Device, Vertices, 24, indices, 36);
}



//円柱
#include <vector>
Geometric_cylinder::Geometric_cylinder(ID3D11Device * p_Device, UINT num) : GeometricPrimitive(p_Device), NUM(num){
	VERTEX3D* vertices = new VERTEX3D[(num + 1) * 2 + num * 4];
	UINT* indices = new UINT[(num * 2 + num * 2) * 3];
	int ofsV = 0, ofsl = 0;
	const float DANGLE = DirectX::XMConvertToRadians(360.0f) / num;
	int wv, wi;

	// 上面
	vertices[ofsV + 0].position = DirectX::XMFLOAT3(0, 0.5f, 0);//中心
	vertices[ofsV + 0].normal = DirectX::XMFLOAT3(0, 1.0f, 0);
	for (UINT i = 0; i < num; i++)
	{
		float a = DANGLE*i;
		vertices[ofsV + 1 + i].position = DirectX::XMFLOAT3(cosf(a)*0.5f, 0.5f, sinf(a)*0.5f);
	}
	for (UINT i = 0; i < num - 1; i++)
	{
		wi = ofsl + 3 * i;
		indices[wi + 0] = ofsV + 0;
		indices[wi + 1] = ofsV + i + 2;
	}
	wi = ofsl + 3 * (num - 1);
	indices[wi + 0] = ofsV + 0;
	indices[wi + 1] = ofsV + 1;

	ofsV += num + 1;
	ofsl += num * 3;

	// 下面
	vertices[ofsV + 0].position = DirectX::XMFLOAT3(0, -0.5f, 0);//中心
	vertices[ofsV + 0].normal = DirectX::XMFLOAT3(0, -1.0f, 0);
	for (u_int i = 0; i < num; i++)
	{
		float a = DANGLE*i;
		vertices[ofsV + 1 + i].position = DirectX::XMFLOAT3(cosf(a)*0.5f, -0.5f, sinf(a)*0.5f);
		vertices[ofsV + 1 + i].normal = DirectX::XMFLOAT3(0, -1.0f, 0);
	}
	for (u_int i = 0; i < num - 1; i++)
	{
		wi = ofsl + 3 * i;
		indices[wi + 0] = ofsV + 0;
		indices[wi + 1] = ofsV + i + 1;
		indices[wi + 2] = ofsV + i + 2;
	}
	wi = ofsl + 3 * (num - 1);
	indices[wi + 0] = ofsV + 0;
	indices[wi + 1] = ofsV + num;
	indices[wi + 2] = ofsV + 1;

	ofsV += num + 1;
	ofsl += num * 3;

	// 側面
	for (u_int i = 0; i < num - 1; i++)
	{
		// 短形１枚ずつ設定
		wv = ofsV + 4 * i;
		wi = ofsl + 6 * i;
		vertices[wv + 0].position = vertices[0 + i + 1].position;
		vertices[wv + 1].position = vertices[0 + i + 2].position;
		vertices[wv + 2].position = vertices[(num + 1) + i + 1].position;
		vertices[wv + 3].position = vertices[(num + 1) + i + 2].position;
		vertices[wv + 0].normal = DirectX::XMFLOAT3(vertices[wv + 0].position.x, 0.0f, vertices[wv + 0].position.z);
		vertices[wv + 1].normal = DirectX::XMFLOAT3(vertices[wv + 1].position.x, 0.0f, vertices[wv + 1].position.z);
		vertices[wv + 2].normal = DirectX::XMFLOAT3(vertices[wv + 0].position.x, 0.0f, vertices[wv + 0].position.z);
		vertices[wv + 3].normal = DirectX::XMFLOAT3(vertices[wv + 1].position.x, 0.0f, vertices[wv + 1].position.z);

		indices[wi + 0] = wv + 0; indices[wi + 1] = wv + 1; indices[wi + 2] = wv + 2;
		indices[wi + 3] = wv + 1; indices[wi + 4] = wv + 3; indices[wi + 5] = wv + 2;
	}
	wv = ofsV + 4 * (num - 1);
	wi = ofsl + 6 * (num - 1);
	vertices[wv + 0].position = vertices[0 + num].position;
	vertices[wv + 1].position = vertices[0 + 1].position;
	vertices[wv + 2].position = vertices[(num + 1) + num].position;
	vertices[wv + 3].position = vertices[(num + 1) + 1].position;
	vertices[wv + 0].normal = DirectX::XMFLOAT3(vertices[wv + 0].position.x, 0.0f, vertices[wv + 0].position.z);
	vertices[wv + 1].normal = DirectX::XMFLOAT3(vertices[wv + 1].position.x, 0.0f, vertices[wv + 1].position.z);
	vertices[wv + 2].normal = DirectX::XMFLOAT3(vertices[wv + 0].position.x, 0.0f, vertices[wv + 0].position.z);
	vertices[wv + 3].normal = DirectX::XMFLOAT3(vertices[wv + 1].position.x, 0.0f, vertices[wv + 1].position.z);

	indices[wi + 0] = wv + 0; indices[wi + 1] = wv + 1; indices[wi + 2] = wv + 2;
	indices[wi + 3] = wv + 1; indices[wi + 4] = wv + 3; indices[wi + 5] = wv + 2;
	ofsV += num * 4;
	ofsl += num * 6;

	create_buffer(p_Device, vertices, ofsV, indices, ofsl);

	delete[] indices;
	delete[] vertices;
}

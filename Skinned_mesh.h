#pragma once

//#include <d3d11.h>
#include <wrl.h>
//#include <DirectXMath.h>
#include "GeometricPrimitive.h"

class Skinned_mesh : public GeometricPrimitive
{
public:
	Skinned_mesh(ID3D11Device* p_Deveice, const char* fbx_filename);
	virtual ~Skinned_mesh();
protected:
	void create_buffer(ID3D11Device* p_Device, VERTEX3D* vertices, const int NUM_VRETEX, UINT* indices, const int NUM_INDEX);
};


#pragma once

#include <d3d11.h>
#include <wrl.h>
#include <DirectXMath.h>
#include <vector>

class Static_mesh{
public:
	struct VERTEX {
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT2 texcoord;
	};
	struct CONSTANT_BUFFER {
		DirectX::XMFLOAT4X4 wvp;  //ワールド・ビュー・プロジェクション合成行列
		DirectX::XMFLOAT4X4 world;      //ワールド変換行列
		DirectX::XMFLOAT4 material_color;    //材質色
		DirectX::XMFLOAT4 light_direction;    //ライト進行方向
	};
	// UNIT.14---------------------
	struct SUBSET {
		std::wstring usemtl;
		u_int index_start = 0;
		u_int index_count = 0;
	};
	std::vector<SUBSET> subset;

	struct MATERIAL {
		std::wstring newmtl;
		DirectX::XMFLOAT3 Ka = { 0.2f,0.2f,0.2f };
		DirectX::XMFLOAT3 Kd = { 0.8f,0.8f,0.8f };
		DirectX::XMFLOAT3 Ks = { 1.0f,1.0f,1.0f };
		u_int illum = 1;
		std::wstring map_Kd;
		ID3D11ShaderResourceView* ShaderResourceView;
	};
	std::vector<MATERIAL> materials;
	// --------------------------------------

	///ID3D11ShaderResourceView* p_ShaderResourceView;
private:
	ID3D11VertexShader* p_VertexShader;
	ID3D11PixelShader* p_PixelShader;
	ID3D11InputLayout* p_InputLayout;

	ID3D11Buffer* p_BufferVs;//（頂点バッファ
	ID3D11Buffer* p_BufferIndex;//（インデックバッファ
	ID3D11Buffer* p_BufferConst;//（定数バッファ

	ID3D11RasterizerState* p_RasterizerStateLine;//（線描画
	ID3D11RasterizerState* p_RasterizerStatePaint;//（塗りつぶし描画
	ID3D11DepthStencilState* p_DepthStencilState;

	//追加
	ID3D11SamplerState* p_SampleState;// サンプラーステート
	///ID3D11Resource* p_Resource;

	int numIndices;//インデックスの数


public:
	Static_mesh(ID3D11Device* p_Device, const wchar_t* obj_filename = nullptr, bool flipping_v_coordinates = false);
	virtual ~Static_mesh();

	//引数
	//p_DeviceContext	:	デバイスコンテキスト
	//wvp			:	ワールド・ビュー・プロジェクション合成行列
	//world			:ワールド変換行列
	//lightVector		:ライト進行方向
	//materialColor	:材質色
	//FlagPaint		:"線or塗りつぶし"描画フラグ
	void render(ID3D11DeviceContext *p_DeviceContext,   //デバイスコンテキスト
		const DirectX::XMFLOAT4X4 &wvp,				//ワールド・ビュー・プロジェクション合成行列
		const DirectX::XMFLOAT4X4 &world,				//ワールド変換行列
		const DirectX::XMFLOAT4 &lightVector,			//ライト進行方向
		const DirectX::XMFLOAT4 &materialColor,			//材質色
		bool  FlagPaint								//線・塗りつぶし描画フラグ
	);
private:
	//引数
	//p_Device	:	デバイス
	//VERTEX3D* vertices:頂点
	//const int NUM_VRETEX:超点数
	//UINT* indices:頂点番号
	//const int NUM_INDEX:頂点番号数
	void Static_mesh::create_buffers(ID3D11Device* p_Device, VERTEX* vertices, const int NUM_VRETEX, u_int* indices, const int NUM_INDEX);
};


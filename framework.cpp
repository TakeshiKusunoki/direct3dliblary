#include "framework.h"
#define CREATE_DEVICE_AND_SWAPCHAIN
///#define PI (3.141592f)
///#define BENCH_NFLAME (8)


bool framework::initialize() {
	//---------------------------------------
	//01追加----------------------------------
	//---------------------------------------

	HRESULT hr = S_OK;
	UINT createDeviceFlag = 0;
#ifdef _DEBUG
	createDeviceFlag |= D3D11_CREATE_DEVICE_DEBUG;
#elif
	createDeviceFlag = D3D11_CREATE_DEVICE_SINGLETHREADED;
#endif
	D3D_DRIVER_TYPE driverTypes[] = {
		//D3D_DRIVER_TYPE_UNKNOWN,
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_REFERENCE,
		D3D_DRIVER_TYPE_WARP,
	};
	UINT numdriverTypes = ARRAYSIZE(driverTypes);//featurelevelsの配列数

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,//DirectX11のみ 9.x以降は無視
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevel = ARRAYSIZE(featureLevels);//featurelevelsの配列数

#ifdef CREATE_DEVICE_AND_SWAPCHAIN
	//スワップ チェーンを記述します。
	DXGI_SWAP_CHAIN_DESC SwapChainDesc;
	ZeroMemory(&SwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	SwapChainDesc.BufferDesc.Width = SCREEN_WIDTH;
	SwapChainDesc.BufferDesc.Height = SCREEN_HEIGHT;
	SwapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	SwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	SwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	SwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	SwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	SwapChainDesc.BufferCount = 1;
	SwapChainDesc.OutputWindow = hwnd;
	SwapChainDesc.SampleDesc.Count = 1;
	SwapChainDesc.SampleDesc.Quality = 0;
	SwapChainDesc.Windowed = TRUE;
	SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	SwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	//デバイスとレンダリングに使用するスワップ チェーンを作成します。
	for (UINT driverTypeIndex = 0; driverTypeIndex < numdriverTypes; driverTypeIndex++) {
		D3D_DRIVER_TYPE drvType = driverTypes[driverTypeIndex];
		D3D_FEATURE_LEVEL featuLevel;
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			drvType,
			NULL,
			createDeviceFlag,
			featureLevels,
			numFeatureLevel,
			D3D11_SDK_VERSION,
			&SwapChainDesc,
			&p_SwapChain,
			&p_Device,
			&featuLevel,
			&p_DeviceContext
		);
		if (SUCCEEDED(hr)) {
			break;
		}
	}

	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr))
		return false;

	//ドライバ消去処理
	hr = p_Device->GetDeviceRemovedReason();
	switch (hr)
	{
	case S_OK:
		break;
	case DXGI_ERROR_DEVICE_HUNG:
	case DXGI_ERROR_DEVICE_RESET:
	case DXGI_ERROR_DEVICE_REMOVED:
	case DXGI_ERROR_DRIVER_INTERNAL_ERROR:
	case DXGI_ERROR_INVALID_CALL:
	default:
		return false;//アプリケーションを終了
		break;
	}
#else //CREATE_DEVICE_AND_SWAPCHAIN
	for (UINT driverTypeIndex = 0; driverTypeIndex < numdriverTypes; driverTypeIndex++) {
		D3D_DRIVER_TYPE drvType = driverTypes[driverTypeIndex];
		D3D_FEATURE_LEVEL featuLevel;
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			drvType,
			NULL,
			createDeviceFlag,
			featureLevels,
			numFeatureLevel,
			D3D11_SDK_VERSION,
			&p_SwapChain,
			&p_Device,
			&featuLevel,
			&p_DeviceContext
		);
		if (SUCCEEDED(hr))break;
	}
	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr))return false;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.BufferDesc.Width = SCREEN_WIDTH;
	swapChainDesc.BufferDesc.Height = SCREEN_HEIGHT;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.OutputWindow = hwnd;
#endif //CREATE_DEVICE_AND_SWAPCHAIN

	// スワップチェインから最初のバックバッファを取得する
	ID3D11Texture2D* pBuckbuffer;
	hr = p_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBuckbuffer);
	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr)) {
		assert(!"スワップチェインから最初のバックバッファを取得するができません");
		return false;
	}


	// 描画ターゲットビューの作成
	hr = p_Device->CreateRenderTargetView(pBuckbuffer, NULL, &p_RenderTargetView);
	pBuckbuffer->Release();
	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr)) {
		assert(!"描画ターゲットビューの作成ができません");
		return false;
	}


	// 深度/ステンシル・テクスチャの作成
	ID3D11Texture2D* pDepthStencilTexture;
	D3D11_TEXTURE2D_DESC DepthDesk;//2D画面分割のデータ
	ZeroMemory(&DepthDesk, sizeof(DepthDesk));
	DepthDesk.Width = SCREEN_WIDTH;
	DepthDesk.Height = SCREEN_HEIGHT;
	DepthDesk.MipLevels = 1;
	DepthDesk.ArraySize = 1;
	DepthDesk.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;//DXGI_FORMAT_D32_FLOAT
	DepthDesk.SampleDesc.Count = 1;
	DepthDesk.SampleDesc.Quality = 0;
	DepthDesk.Usage = D3D11_USAGE_DEFAULT;
	DepthDesk.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	DepthDesk.CPUAccessFlags = 0;
	DepthDesk.MiscFlags = 0;
	hr = p_Device->CreateTexture2D(&DepthDesk, NULL, &pDepthStencilTexture);//作成する２Dテクスチャの設定
	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr)) {
		assert(!"深度/ステンシル・テクスチャの作成ができません");
		return false;
	}


	//　深度/ステンシルビューの作成
	D3D11_DEPTH_STENCIL_VIEW_DESC DescDepthStencilView;
	ZeroMemory(&DescDepthStencilView, sizeof(DescDepthStencilView));
	DescDepthStencilView.Format = DepthDesk.Format;
	DescDepthStencilView.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	DescDepthStencilView.Texture2D.MipSlice = 0;
	DescDepthStencilView.Flags = 0;
	hr = p_Device->CreateDepthStencilView(pDepthStencilTexture, &DescDepthStencilView, &p_DepthStencilView);
	pDepthStencilTexture->Release();
	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr)) {
		assert(!"深度/ステンシルビューの作成ができません");
		return false;
	}
	/////////////////////////////////////
	////Added by Unit7
	/////////////////////////////////////
	BlendMode::Initializer(p_Device);

	//---------------------------
	////////////////////////////////////////
	//Added by Unit10
	///////////////////////////////////////
	//cube = new Geometric_cylinder(p_Device);
	mesh = new Static_mesh(p_Device, L"Mr.Incredible.obj");
	return true;
}
void framework::update(float elapsed_time/*Elapsed seconds from last frame*/) {

}

void framework::render(float elapsed_time/*Elapsed seconds from last frame*/) {

	// 描画ターゲットのクリア
	float ClearColor[4] = { 0.0f,0.125f,0.3f,1.0f };//クリアする値
	p_DeviceContext->ClearRenderTargetView(p_RenderTargetView, ClearColor);
	// 深度ステンシル リソースをクリアします。
	p_DeviceContext->ClearDepthStencilView(p_DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

#define VIEW_NUM 1
	// 画面描画ターゲットの領域の設定		viewport(ビューポートの寸法を定義します。)ビューポートは -1.0〜1.0 の範囲で作られたワールド座標をスクリーン座標（表示するウインドウのサイズ）に変換するための情報)//
	D3D11_VIEWPORT Viewport[VIEW_NUM];
	Viewport[0].TopLeftX = 0;
	Viewport[0].TopLeftY = 0;
	Viewport[0].Width = static_cast<FLOAT>(SCREEN_WIDTH);
	Viewport[0].Height = static_cast<FLOAT>(SCREEN_HEIGHT);//1lo0O10O0O8sSBloO
	Viewport[0].MinDepth = 0.0f;
	Viewport[0].MaxDepth = 1.0f;
	p_DeviceContext->RSSetViewports(VIEW_NUM, Viewport);

	// 最後に　描画ターゲット・ビューの設定
	p_DeviceContext->OMSetRenderTargets(VIEW_NUM, &p_RenderTargetView, p_DepthStencilView);

#undef VIEW_NUM


	/////////////////////////////////////
	////Added by Unit10
	/////////////////////////////////////
	using namespace DirectX;
	// �@ローカル座標
	static XMFLOAT3 pos(0,-1.5f,0);//オブジェクトの位置
	static XMFLOAT3 angle(0, 0, 0);//オブジェクトの角度
	static XMFLOAT3 scale(1.5, 1.5, 1.5);//オブジェクトの大きさ
	//angle.x += XMConvertToRadians(0.1f);
	//angle.y += XMConvertToRadians(0.1f);
	//angle.z += XMConvertToRadians(0.1f);
	pos.y -= 0.001f;
	// �Aワールド座標
	DirectX::XMMATRIX S, R, T;
	S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//拡大縮小行列
	R = DirectX::XMMatrixRotationX(angle.x);//回転行列
	R = DirectX::XMMatrixRotationY(angle.y);
	R = DirectX::XMMatrixRotationZ(angle.z);
	T = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);//平行移動行列
	DirectX::XMMATRIX W = S * R * T;

	// �Bビュー座標
	static DirectX::XMFLOAT4 cameraPos = { 0,0,-10,1 };
	static DirectX::XMFLOAT4 cameraTarget = { 0,0,0,1 };
	const DirectX::XMFLOAT4 normal = { 0,1,0,0 };//上方向[
	cameraPos.x += 0.001f;
	//cameraPos.y -= 0.001f;
	//cameraPos.z -= 0.001f;
	XMVECTOR eye = DirectX::XMLoadFloat4(&cameraPos);
	XMVECTOR focus = DirectX::XMLoadFloat4(&cameraTarget);
	XMVECTOR up = DirectX::XMLoadFloat4(&normal);
	XMMATRIX viewMatrix = XMMatrixLookAtLH(eye, focus, up);

	// �Cプロジェクション座標
	static float fovY = DirectX::XMConvertToRadians(30.0f);//(カメラの視野角)
	float    aspect = (float)SCREEN_W / (float)SCREEN_H;
	float    nearZ = 0.1f;
	float    farZ = 200.0f;
	XMMATRIX projMatrix = XMMatrixPerspectiveFovLH(fovY, aspect, nearZ, farZ);//透視投影行列

	// �Dワールド・ビュー・プロジェクション行列（WVP）を合成する
	XMFLOAT4X4 WVP, w;
	DirectX::XMStoreFloat4x4(&WVP, W * viewMatrix * projMatrix);
	DirectX::XMStoreFloat4x4(&w, W);

	// �Eライト方向・材質色を設定する
	static DirectX::XMFLOAT4 lightVectol = { 0,-1,0,0 };
	static DirectX::XMFLOAT4 materialColor = { 1,1,1,1 };

	// 描画
	//cube->render(p_DeviceContext, WVP, w, lightVectol, materialColor, true);
	mesh->render(p_DeviceContext, WVP, w, lightVectol, materialColor, true);

	// レンダリングされたイメージをユーザーに表示します。
	BlendMode::Set(p_DeviceContext, BlendMode::NONE);
	HRESULT hr;
	hr = p_SwapChain->Present(0, 0);
	//StanbyPresant();

	return;
}

// 無駄な画面描画の抑制
bool framework::StanbyPresant() {
	static bool standByMode = false;//スタンバイ状態を管理するstatic変数
	HRESULT hr;
	// スタンバイモード
	if (standByMode) {
		hr = p_SwapChain->Present(0, DXGI_PRESENT_TEST);
		if (hr == DXGI_STATUS_OCCLUDED) {
			// 描画する必要がない(表示領域がない)
			return standByMode;
		}
		standByMode = false;//スタンバイ・モードを解除
	}
	// 画面の更新
	hr = p_SwapChain->Present(0, 0);
	if (hr == DXGI_STATUS_OCCLUDED)
		standByMode = true;//スタンバイ・モードに入る
	return standByMode;
}


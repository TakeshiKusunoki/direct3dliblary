// UNIT.16
#include "Skinned_mesh.h"
#include <fbxsdk.h>
#include <vector>
#include <functional>
//#include <fbxsdk\fileio\fbxiosettingspath.h>


Skinned_mesh::Skinned_mesh(ID3D11Device * p_Deveice, const char * fbx_filename)
{
	// Create the FBX SDK manager
	fbxsdk::FbxManager* Manager = fbxsdk::FbxManager::Create();

	// Create an IOStettings object. IOSROOT is defined in Fbxiosettingspath.h.
	Manager->SetIOSettings(fbxsdk::FbxIOSettings::Create(Manager, IOSROOT));

	// Create an importer.
	fbxsdk::FbxImporter* importer = fbxsdk::FbxImporter::Create(Manager, "");

	// intialize the importer.
	bool import_status = false;
	import_status = importer->Initialize(fbx_filename, -1, Manager->GetIOSettings());
	_ASSERT_EXPR(import_status, importer->GetStatus().GetErrorString());

	// Create a new scene so it can be populated by the imported file.
	fbxsdk::FbxScene* Scene = fbxsdk::FbxScene::Create(Manager, "");

	// Import the contents of the file into the scene.
	import_status = importer->Import(Scene);
	_ASSERT_EXPR(import_status, importer->GetStatus().GetErrorString());

	// Convert mesh, NURBS and patch into triangle mesh
	fbxsdk::FbxGeometryConverter Geometry_converter(Manager);
	Geometry_converter.Triangulate(Scene,/*replace*/true);

	// Fetch node attributes and materials under this node recursively.Currentry only mesh.
	std::vector<fbxsdk::FbxNode*> Fetched_meshes;
	std::function<void(fbxsdk::FbxNode*)>Traverse = [&](fbxsdk::FbxNode* Node) {
		if (Node)
		{
			fbxsdk::FbxNodeAttribute* Fbx_node_attribute = Node->GetNodeAttribute();
			if (Fbx_node_attribute)
			{
				switch (Fbx_node_attribute->GetAttributeType())
				{
				case fbxsdk::FbxNodeAttribute::eMesh:
					Fetched_meshes.push_back(Node);
					break;
				default:
					break;
				}
			}
			for (int i = 0; i < Node->GetChildCount(); i++)
			{
				Traverse(Node->GetChild(i));
			}
		}
	};
	Traverse(Scene->GetRootNode());

	fbxsdk::FbxMesh* Fbx_mesh = Fetched_meshes.at(0)->GetMesh();// Currently only one mesh.

	// Fetch mesh data
	std::vector<VERTEX3D> vertices;// Vertex buffer
	std::vector<u_int> indices;// Index buffer
	u_int vertex_count = 0;

	const fbxsdk::FbxVector4* array_of_contorol_points = Fbx_mesh->GetControlPoints();
	const int number_of_polygons = Fbx_mesh->GetPolygonCount();
	for (int index_of_poligon = 0; index_of_poligon < number_of_polygons; index_of_poligon++)
	{
		for (int index_of_vertex = 0; index_of_vertex < 3; index_of_vertex++)
		{
			VERTEX3D vertexCopy;
			const int index_of_contorol_point = Fbx_mesh->GetPolygonVertex(index_of_poligon, index_of_vertex);
			vertexCopy.position.x = static_cast<float>(array_of_contorol_points[index_of_contorol_point][0]);
			vertexCopy.position.y = static_cast<float>(array_of_contorol_points[index_of_contorol_point][1]);
			vertexCopy.position.z = static_cast<float>(array_of_contorol_points[index_of_contorol_point][2]);

			fbxsdk::FbxVector4 normal;
			Fbx_mesh->GetPolygonVertexNormal(index_of_poligon, index_of_vertex, normal);
			vertexCopy.normal.x = static_cast<float>(normal[0]);
			vertexCopy.normal.y = static_cast<float>(normal[1]);
			vertexCopy.normal.z = static_cast<float>(normal[2]);

			vertices.push_back(vertexCopy);
			indices.push_back(vertex_count);
			vertex_count += 1;
		}
	}
	Manager->Destroy();
	// Initialize Direct3D COM objects

}

Skinned_mesh::~Skinned_mesh()
{
}

void Skinned_mesh::create_buffer(ID3D11Device * p_Device, VERTEX3D * vertices, const int NUM_VRETEX, UINT * indices, const int NUM_INDEX)
{
}

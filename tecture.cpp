#include "tecture.h"
#include "misc.h"
#include "DirectXTK-master\Inc\WICTextureLoader.h"
#include <wrl.h>
#include <map>

// UNIT13
HRESULT load_texture_from_file(ID3D11Device* p_Device, const wchar_t* file_name, ID3D11ShaderResourceView** ShaderResourceView, D3D11_TEXTURE2D_DESC* Texture2DDesc)
{
	HRESULT hr = S_OK;
	Microsoft::WRL::ComPtr<ID3D11Resource> resource;
	static std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> cache;
	auto it = cache.find(file_name);
	if (it!=cache.end())
	{
		*ShaderResourceView = it->second.Get();
		(*ShaderResourceView)->AddRef();
		(*ShaderResourceView)->GetResource(resource.GetAddressOf());
	}
	else
	{
		hr = DirectX::CreateWICTextureFromFile(p_Device, file_name, resource.GetAddressOf(), ShaderResourceView);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		cache.insert(std::make_pair(file_name, *ShaderResourceView));
	}
	Microsoft::WRL::ComPtr<ID3D11Texture2D> texture2d;
	hr = resource.Get()->QueryInterface<ID3D11Texture2D>(texture2d.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	texture2d->GetDesc(Texture2DDesc);

	return hr;
}

/*
e.g.
obj_filename <= L"data/bison.obj"
resource_filename <= L"/user/textures/bison.png"
combined_resource_path => L"/data/bison.png"
*/
void CombineResourcePath(wchar_t(&combind_resource_path)[256], const wchar_t* obj_fileName, const wchar_t* resource_fileName)
{
	const wchar_t delimiters[] = { L'\\', L'/' };
	// obj_filename からディレクトリを抽出する
	wchar_t directory[256] = {};
	for (wchar_t it : delimiters)
	{
		wchar_t* p = wcsrchr(const_cast<wchar_t*>(obj_fileName), it);//ストリング内でワイド文字が最後に現れる位置の検出
		if (p)
		{
			// ディレクトリに名前コピー
			memcpy_s(directory, 256, obj_fileName,(p-obj_fileName+1)*sizeof(wchar_t));
			break;
		}
	}
	// resource_filename からファイル名を抽出
	wchar_t filename[256];
	wcscpy_s(filename, resource_fileName);
	for (wchar_t it : delimiters)
	{
		wchar_t* p = wcsrchr(filename, it);//ストリング内でワイド文字が最後に現れる位置の検出
		if (p)
		{
			wcscpy_s(filename, p + 1);//ワイド文字列をコピーする。
			break;
		}
	}
	// ディレクトリとファイル名を結合する
	wcscpy_s(combind_resource_path, directory);
	wcscat_s(combind_resource_path, filename);

}

